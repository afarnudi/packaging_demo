.. Kiarash documentation master file, created by
   sphinx-quickstart on Fri Jul 12 14:05:28 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kiarash's fantasy documentation!
===========================================

Hi my name is Kiarash. Welcome to my webpage.

.. autofunction:: src.hello.say_hello

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
