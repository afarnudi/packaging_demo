import argparse


def say_hello(name="world"):
    """
    Generates a greeting message.

    This function takes a name as input and returns a greeting message in the format "Hello {name}!".

    Parameters
    ----------
    name : str, optional
        The name to be included in the greeting message. Default is "world".

    Returns
    -------
    str
        A greeting message in the format "Hello {name}!".

    Notes
    -----
    If no name is provided, the function defaults to using "world".

    Examples
    --------
    >>> say_hello()
    'Hello world!'

    >>> say_hello("Alice")
    'Hello Alice!'
    """ 
    return f"Hello {name}!"


def main():
    parser = argparse.ArgumentParser(
        prog="kiarash",
        description="Welcome to the Tissue Deformation Quantifier (TDQ). "
        "TDQ can visualise and quantify tissue's cell deformation using labeled images and property files."
        "To begin, process the labelled images or perform analysis on the processed data.",
    )
    parser.add_argument(
        "-n",
        "--name",
        type=str,
        default="world",
        help="Name displayed after hello.",
    )
    parser.add_argument(
        "-i",
        "--int",
        type=int,
        required=True,
        help="some number",
    )
    args = parser.parse_args()
    print(say_hello(args.name))


if __name__ == "__main__":
    main()
